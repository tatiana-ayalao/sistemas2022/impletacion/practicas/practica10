﻿USE ciclistas;

/** consulta 1 Numero de ciclistas que hay
  **/

 SELECT COUNT(*) AS nciclistas
FROM ciclista c;


/** Numero de ciclistas que hay del equipo Banesto
  **/


  SELECT COUNT(*)
     FROM ciclista c
    WHERE
    c.nomequipo='Banesto';

/** consulta 3 La edad media de los ciclistas
  **/
SELECT AVG(edad) AS edadMedia
FROM ciclista c;




/** consulta 4 La edad media de los de equipo Banesto
  **/

SELECT AVG(edad) AS edadMedia
FROM ciclista c
  WHERE
  c.nomequipo='Banesto';



/** consulta 5 La edad media de los ciclistas 
  por cada equipo
**/

  SELECT 
    c.nomequipo,AVG(edad) AS edadMedia
FROM ciclista c
    GROUP BY
    c.nomequipo;


/** consulta 6 El número total de puertos
  **/

  SELECT COUNT(*), c.nomequipo  
  FROM ciclista c
    GROUP BY
    c.nomequipo;

/** consulta 7 El número total de puertos 
  mayores de 1500 **/

SELECT 
  COUNT(*) AS nPuertos
FROM puerto p
  WHERE
  p.altura>1500;

/**Listar el nombre de los equipos 
  que tengan más de 4 ciclistas **/

SELECT COUNT(*) AS nciclistas, c.nomequipo 
FROM ciclista c
  GROUP BY
  c.nomequipo;


/**Listar el nombre de los equipos que tengan 
  más de 4 ciclistas cuya edad este entre 28
y 32 **/

  SELECT c.nomequipo
     FROM ciclista c
    WHERE
    c.edad BETWEEN 28 AND 32
    GROUP BY
    c.nomequipo
    HAVING COUNT(*)>4;

/** CONSULTA 10 Indícame e el número de etapas 
  que ha ganado cada uno de los ciclistas
  **/

  SELECT dorsal, COUNT(*) AS nEtapas
     FROM etapa e
    GROUP BY
    e.dorsal;


  /** consulta 11 indicame el dorsal de los ciclistas que 
    hayan ganado más de una etapa**/

    SELECT e.dorsal 
    FROM etapa e
    GROUP BY
    e.dorsal
    HAVING
    COUNT(*)>1;